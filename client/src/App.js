import React, { Component } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import cookie from 'react-cookies';
import './style/main.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Main from './components/body/main';
import Dashboard from './components/body/dashboard';
import Header from './components/header/header';
import Footer from './components/footer/footer';

// importing constants
import { base_url } from './config/const';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null,
      login: false,
      status: null,
      name: null,
      permission: null
    }
  }

  // No initial mount
  componentDidMount() {
    let usersName = cookie.load('name');
    let userPermission = cookie.load('permission');
    if(usersName && userPermission) { this.setState({ name: usersName, permission: userPermission }); }
  }

  // Set User State
  setStateValue = (key,value) => {
    this.setState({[key]:value});
  };

  // User Login
  userLogin = () => {
    // Check if the email and password are filled
    if(!this.state.email || !this.state.password) return;
    // Body of the API call
    let body = {
      email: this.state.email,
      password: this.state.password
    };

    fetch(`${base_url}api/auth/login`,{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(
        // Results ..
        (result) => {
          // If authenticated
          if(result.login){
            // State changed
            this.setState({ login: result.login, status: result.message, email: null, password: null, permissions: result.permission, name: result.name });
            // Auth cookie created for 6 hours
            cookie.save('auth', result.token, { path: '/', maxAge: (6*3600), secure: false, httpOnly: false });
            // Name cookie for 6 hours
            cookie.save('name', result.name, { path: '/', maxAge: (6*3600), secure: false, httpOnly: false });
            // Permission cookie for 6 hours
            cookie.save('permission', result.permission, { path: '/', maxAge: (6*3600), secure: false, httpOnly: false });
          }
          if(!result.login) {
            this.setState({ login: result.login, status: result.message });
          }
        },
        // Any errors
        (error) => {
          this.setState({
            login: false,
            status: error.message
          });
        }
      )
  };

  // logout
  userLogout = () => {
    this.setState({
      login: false,
      status: 'Logged out.',
      permission: null,
      name: null
    });
    cookie.remove('name', { path: '/' });
    cookie.remove('permission', { path: '/' });
    cookie.remove('auth', { path: '/' });
  }
  
  // Get work order
  getWorkOrder = async(reqdata={}) => {
    // Getting auth token
    let authToken = cookie.load('auth');
    if(!authToken) return;
    // Building body
    let body = {};
    // If there is vehicle id
    if(reqdata.vehicleid !== undefined) body.vehicleid = reqdata.vehicleid;
    // If there is work order id
    if(reqdata._id !== undefined) body._id = reqdata._id;
    // If there is status
    if(reqdata.status !== undefined) body.status = reqdata.status;
    // If there is skip
    if(reqdata.skip !== undefined) body.skip = reqdata.skip;

    return await fetch(`${base_url}api/service/get`,{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'x-auth-token': authToken
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(
        // Results ..
        (result) => {
          // If authenticated
          if(result.status){
            // If status true
            return result;
          }
          if(!result.status) {
            // If status is false
            // remove the cookies
            cookie.remove('name', { path: '/' });
            cookie.remove('permission', { path: '/' });
            cookie.remove('auth', { path: '/' });
            return result;
          }
        },
        // Any errors
        (error) => {
          return ({ status: false, message: error.message });
        }
      )
  };

  // Update work order
  updateWorkOrder = async(reqdata={}) => {
    // Getting auth token
    let authToken = cookie.load('auth');
    if(!authToken) return;
    // Building body
    let body = {};
    // If there is vehicle id
    if(reqdata.description !== undefined) body.description = reqdata.description;
    // If there is work order id
    if(reqdata._id !== undefined) body._id = reqdata._id;
    // If there is status
    if(reqdata.status !== undefined) body.status = reqdata.status;
    // If there is cost
    if(reqdata.cost !== undefined) body.cost = reqdata.cost;

    return await fetch(`${base_url}api/service/update`,{
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'x-auth-token': authToken
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(
        // Results ..
        (result) => {
          // If authenticated
          if(result.status){
            // If status true
            return result;
          }
          if(!result.status) {
            // remove the cookies
            cookie.remove('name', { path: '/' });
            cookie.remove('permission', { path: '/' });
            cookie.remove('auth', { path: '/' });
            // If status is false
            return result;
          }
        },
        // Any errors
        (error) => {
          return ({ status: false, message: error.message });
        }
      )
  };

  // Create work orderBy
  createWorkOrder = async(reqdata={}) => {
    // Getting auth token
    let authToken = cookie.load('auth');
    if(!authToken) return;

    let bodydata = reqdata;

    return await fetch(`${base_url}api/service/create`,{
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'x-auth-token': authToken
        },
        body: JSON.stringify(bodydata)
      })
      .then(res => res.json())
      .then(
        // Results ..
        (result) => {
          // If authenticated
          if(result.status){
            // If status true
            return result;
          }
          if(!result.status) {
            // remove the cookies
            cookie.remove('name', { path: '/' });
            cookie.remove('permission', { path: '/' });
            cookie.remove('auth', { path: '/' });
            // If status is false
            return result;
          }
        },
        // Any errors
        (error) => {
          return ({ status: false, message: error.message });
        }
      )
  };

  // Delete Work order
  deleteWorkOrder = async(reqdata={}) => {
    // Getting auth token
    let authToken = cookie.load('auth');
    if(!authToken) return;
    // Building body
    let body = {};
    // If there is work order id
    if(reqdata._id !== undefined) body._id = reqdata._id;

    return await fetch(`${base_url}api/service/delete`,{
        method: 'DELETE',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'x-auth-token': authToken
        },
        body: JSON.stringify(body)
      })
      .then(res => res.json())
      .then(
        // Results ..
        (result) => {
          // If authenticated
          if(result.status){
            // If status true
            return result;
          }
          if(!result.status) {
            // If status is false
            // remove the cookies
            cookie.remove('name', { path: '/' });
            cookie.remove('permission', { path: '/' });
            cookie.remove('auth', { path: '/' });
            return result;
          }
        },
        // Any errors
        (error) => {
          return ({ status: false, message: error.message });
        }
      )
  };

  render() {
    const usersName = cookie.load('name');
    return (
      <Container className="container-fluid">
        <br/>
        <Header setStateValue={this.setStateValue.bind(this)} userLogin={this.userLogin.bind(this)} status={this.state.status} name={this.state.name} permission={this.state.permission} userLogout={this.userLogout.bind(this)} />
        <br/>
        
          <Routes>
            <Route path="/" element={<Main status={this.state.status} />} />
            <Route path="/dashboard" element={(usersName)?<Dashboard status={this.state.status} getWorkOrder={this.getWorkOrder.bind(this)} deleteWorkOrder={this.deleteWorkOrder.bind(this)} updateWorkOrder={this.updateWorkOrder.bind(this)} createWorkOrder={this.createWorkOrder.bind(this)} />:<Navigate to="/" />} />
          </Routes>
        <Footer />
      </Container>
    );
  }
}

export default App;
