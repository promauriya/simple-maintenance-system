import React, { Component } from 'react';
import { Container, Row, Col, ListGroup, Button, Spinner, Form, Alert } from 'react-bootstrap';
import cookie from 'react-cookies';
import '../../style/main.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
            skip: 0,
            name: null,
            sidebar: false,
            sdata: null,
            sstatus: null,
            sdescription: null,
            scost: null,
            s_id: null,
            status: null,
            page: null,
            fleet: null,
            vehicleID: null

        }
    }

    componentDidMount() {
        // Find cookie and set state
        this.gatherWorks();
        this.interval = setInterval(() =>{this.gatherWorks();}, 3000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    // Get work order
    gatherWorks = async() => {
        let wdata = await this.props.getWorkOrder();
        if(!wdata) return;
        if(wdata.data === null) return;
        if(wdata.data === undefined) return;
        if(wdata.data.length>0) this.setState({data: wdata.data});
        console.log(wdata.data);
    }

    ifSelected = (key,value) => {
        if(key===value) return 'selected';
    }

    // Set data State
    setStateValue = (key,value) => {
        this.setState({[key]:value});
    };

    // Edit work order
    editWork = () => {
        if(this.state.sidebar && this.state.sdata && this.state.page==='edit') {
            // Single data
            let sdata = this.state.sdata;
            return (
                <Form>
                    <h2>Edit Work Order for {sdata.vehicleID}</h2>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="">
                            <strong>Vehicle</strong>
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control plaintext readOnly defaultValue={sdata.vehicleID} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="2">
                            <strong>Fleet</strong>
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control plaintext readOnly defaultValue={sdata.fleet} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="2">
                            <strong>Created By</strong>
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control plaintext readOnly defaultValue={sdata.assigneeID} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="1">
                            <strong>Status</strong>
                        </Form.Label>
                        <Form.Control as="select" name="sstatus" value={(this.state.sstatus)?this.state.sstatus:sdata.status} onChange={(e)=>{this.setStateValue('sstatus',e.target.value)}}>
                            <option value="open">Open</option>
                            <option value="pending">Pending</option>
                            <option value="waiting for parts">Waiting for Parts</option>
                            <option value="completed">Completed</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows={3} value={(this.state.sdescription)?this.state.sdescription:sdata.description} onChange={(e)=>{this.setStateValue('sdescription',e.target.value)}} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Cost</Form.Label>
                        <Form.Control type="email" value={(this.state.scost)?this.state.scost:sdata.cost} onChange={(e)=>{this.setStateValue('scost',e.target.value)}} />
                    </Form.Group>
                    {(this.state.status)?<Alert variant="primary">{this.state.status}</Alert>:''}
                    <Form.Group as={Row} className="mb-3">
                        <Col sm={{ span: 10 }}>
                        <Button type="submit" className="ten-space" onClick={(e)=>{e.preventDefault();this.updateWork()}}>Change</Button>
                        <Button  className="ten-space" onClick={(e)=>{e.preventDefault();this.setState({sidebar:false,status:null,sstatus:null,sdescription:null,scost:null,page:null})}}>Cancel</Button>
                        </Col>
                    </Form.Group>
                </Form>
            )
        }
    }

    // Create work order
    createWork = () => {
        if(this.state.sidebar && this.state.page==='create') {
            return (
                <Form>
                    <h2>Create Work Order</h2>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="">
                            <strong>Vehicle</strong>
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control type="vehicle" onChange={(e)=>{this.setStateValue('vehicleID',e.target.value)}} />
                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="1">
                            <strong>Status</strong>
                        </Form.Label>
                        <Form.Control as="select" name="sstatus" onChange={(e)=>{this.setStateValue('sstatus',e.target.value)}}>
                            <option value="">Choose</option>
                            <option value="open">Open</option>
                            <option value="pending">Pending</option>
                            <option value="waiting for parts">Waiting for Parts</option>
                            <option value="completed">Completed</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" rows={3} onChange={(e)=>{this.setStateValue('sdescription',e.target.value)}} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                        <Form.Label>Cost</Form.Label>
                        <Form.Control type="email" onChange={(e)=>{this.setStateValue('scost',e.target.value)}} />
                    </Form.Group>
                    {(this.state.status)?<Alert variant="primary">{this.state.status}</Alert>:''}
                    <Form.Group as={Row} className="mb-3">
                        <Col sm={{ span: 10 }}>
                        <Button type="submit" className="ten-space" onClick={(e)=>{e.preventDefault();this.createWorkO()}}>Create</Button>
                        <Button  className="ten-space" onClick={(e)=>{e.preventDefault();this.setState({sidebar:false,status:null,sstatus:null,sdescription:null,scost:null,page:null,vehicleID:null,assigneeID:null})}}>Cancel</Button>
                        </Col>
                    </Form.Group>
                </Form>
            )
        }
    }

    // Delete work order
    deleteWork = (sdata) => {
        if(this.state.sidebar && this.state.sdata && this.state.page==='delete') {
            // Single data
            let sdata = this.state.sdata;
            return (
                <Form>
                    <h2>Delete Work Order</h2>
                    <Form.Group as={Row} className="mb-3" controlId="formPlaintextEmail">
                        <Form.Label column sm="">
                            <strong>Would you like to delete work order {sdata.vehicleID}</strong>
                        </Form.Label>
                        <Col sm="10">
                            <Form.Control plaintext readOnly defaultValue={sdata.vehicleID} />
                        </Col>
                    </Form.Group>
                    {(this.state.status)?<Alert variant="primary">{this.state.status}</Alert>:''}
                    <Form.Group as={Row} className="mb-3">
                        <Col sm={{ span: 10 }}>
                        {this.state.status?'':<Button type="submit" className="ten-space" onClick={(e)=>{e.preventDefault();this.deletetheWork()}}>Delete</Button>}
                        {this.state.status?<Button  className="ten-space" onClick={(e)=>{e.preventDefault();this.setState({sidebar:false,status:null,sstatus:null,sdescription:null,scost:null,page:null,sdata:null})}}>OK</Button>:<Button  className="ten-space" onClick={(e)=>{e.preventDefault();this.setState({sidebar:false,status:null,sstatus:null,sdescription:null,scost:null,page:null})}}>Cancel</Button>}
                        </Col>
                    </Form.Group>
                </Form>
            )
        }
    }

    // Create work order
    createWorkO = async() => {
        if(!this.state.sstatus || !this.state.sdescription || !this.state.vehicleID || !this.state.scost)return;
        let data = {
            status: this.state.sstatus,
            description: this.state.sdescription,
            vehicleid: this.state.vehicleID,
            cost: this.state.scost
        }
        let returndata = await this.props.createWorkOrder(data);
        this.setState({status: returndata.message});
    }

    // update work order
    updateWork = async() => {
        if(!this.state.s_id)return;
        let data = {_id: this.state.s_id}
        if(this.state.sstatus)data.status = this.state.sstatus;
        if(this.state.sdescription)data.description = this.state.sdescription;
        if(this.state.scost)data.cost = this.state.scost;
        let returndata = await this.props.updateWorkOrder(data);
        this.setState({status: returndata.message});
    }

    // delete work order
    deletetheWork = async() => {
        if(!this.state.s_id)return;
        let data = {_id: this.state.s_id}
        let returndata = await this.props.deleteWorkOrder(data);
        this.setState({status: returndata.message,sstatus:null,sdescription:null,scost:null,s_id:null});
    }

    // Render work order
    renderWorkOrder = () => {
        if(this.state.data) {
            let data = this.state.data;
            var self = this;
            var userPermission = parseInt(cookie.load('permission'));
            return (
                <ListGroup>
                    { 
                        data.map((element, index) => {
                            let deleteButton = (userPermission === 3?(<Button className="dashboard-button" onClick={(e)=>{e.preventDefault();self.setState({sidebar:true,sdata:element,s_id:element._id,page:'delete',status:null})}}>Delete</Button>):(''));
                            let editButton = (userPermission >= 2?(<Button className="dashboard-button" onClick={(e)=>{e.preventDefault();self.setState({sidebar:true,sdata:element,s_id:element._id,status:null,sstatus:null,sdescription:null,scost:null,page:'edit'})}}>Edit</Button>):(''));
                            return (
                            <ListGroup.Item key={index}>
                                {deleteButton} {editButton}
                                <strong>Vehicle ID:</strong> {(element.vehicleID)?element.vehicleID:''}<br/>
                                <strong>Fleet:</strong> {(element.fleet)?element.fleet:''}<br/>
                                <strong>Description:</strong> {(element.description)?element.description:''}<br/>
                                <strong>Cost:</strong> {(element.cost)?element.cost:''}<br/>
                                <strong>Created By:</strong> {(element.assigneeID)?element.assigneeID:''}<br/>
                                <strong>Updated:</strong> {(element.lastupdated)?element.lastupdated:''}<br/>
                                <strong>Status:</strong> {(element.status)?element.status:''}
                            </ListGroup.Item>)
                        })
                    }
                </ListGroup>
            )
        }
        if(this.state.data === null) {
            return (<ListGroup><Spinner animation="border" role="status"><span className="visually-hidden">Loading...</span></Spinner></ListGroup>)
        }
    }

    render() {
        const userPermission = cookie.load('permission');
        return (
            <Container fluid>
                <Row className="justify-content-md-center">
                    <Col className="top-home-page-box">
                        {(this.props.status)?<center>{this.props.status}</center>:''}
                        <h2>Dashboard {(userPermission>=2 && !this.state.page?(<Button className="dashboard-button" onClick={(e)=>{e.preventDefault();this.setState({sidebar:true,sdata:null,s_id:null,status:null,sstatus:null,sdescription:null,scost:null,page:'create'})}}>Create</Button>):(''))}
                        </h2>
                        <p></p>
                        {this.renderWorkOrder()}
                    </Col>
                    {(this.state.sidebar && this.state.page==='edit')?<Col className="top-home-page-box">{this.editWork()}</Col>:''}
                    {(this.state.sidebar && this.state.page==='delete')?<Col className="top-home-page-box">{this.deleteWork()}</Col>:''}
                    {(this.state.sidebar && this.state.page==='create')?<Col className="top-home-page-box">{this.createWork()}</Col>:''}
                </Row>
            </Container>
        );
    }
}

export default Dashboard;