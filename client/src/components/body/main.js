import React, { Component } from 'react';
import { Container, Row, Col, Alert } from 'react-bootstrap';
import '../../style/main.css';

class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <Container fluid>
                <Row className="justify-content-md-center">
                    <Col className="top-home-page-box">
                        {(this.state.status)?<Alert variant="primary">{this.state.status}</Alert>:''}
                        <h2>Simple Maintenance System</h2>
                        <p>Simple maintenance management system for certain industries where total cost of ownership or total operating costs is a substantial percentage of revenue, the maintenance process should be tracked in order to minimize costs of assets.</p>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Main;