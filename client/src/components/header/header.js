import React, { Component } from 'react';
import { Container, Navbar, Nav, Form, Button, Popover, OverlayTrigger, Alert } from 'react-bootstrap';
// import { Link } from "react-router-dom";
// importing style sheet
import '../../style/main.css';

class Navi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            
        }
    }

    loginOut = () => {
        const poplogin = (
            <Popover id="popover-basic">
                <Popover.Header as="h3">User Login</Popover.Header>
                <Popover.Body>
                <Form>
                    <Form.Group controlId="formBasicText">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="text" placeholder="Enter Email" onChange={(e)=>{this.props.setStateValue('email',e.target.value)}} />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" onChange={(e)=>{this.props.setStateValue('password',e.target.value)}} />
                    </Form.Group>
                    <Form.Group controlId="formBasicChecbox">
                        <Form.Check type="checkbox" label="Check me out" />
                    </Form.Group>
                    {(this.props.status)?<Form.Group controlId="formBasicText"><Form.Label><Alert variant={'success'}>{this.props.status}</Alert></Form.Label></Form.Group>:''}
                    <Button variant="primary" type="submit" onClick={()=>{this.props.userLogin()}}>
                        Submit
                    </Button>
                </Form>
                </Popover.Body>
            </Popover>
        );
        if(!this.props.name) {
            return(
                <OverlayTrigger trigger="click" placement="bottom" overlay={poplogin}>
                    <Button variant="secondary">Login</Button>
                </OverlayTrigger>
            );
        }
        if(this.props.name) {
            return(<Button variant="secondary" onClick={()=>{this.props.userLogout()}}>Logout</Button>);
        }
    }

    render() {
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container className="container">
                <Navbar.Brand href="#home">Simple Maintenance System</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        {(this.props.name)?<Nav.Link href="/dashboard">Dashboard</Nav.Link>:''}
                    </Nav>
                    <Nav>
                    {(this.props.name)?<Nav.Link className="header-name">{'Hi '+this.props.name}</Nav.Link>:''}
                    <Nav.Link>{this.loginOut()}</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                </Container>
            </Navbar>
        );
    }
}

export default Navi;