const bcrypt = require('bcryptjs');

// Salt Round
const saltRounds = 10;

class PassCode {
    constructor(password){
        this.password = password ;
        this.processpass();
    }
    processpass() {
        // creating hash
        var salt = bcrypt.genSaltSync(saltRounds);
        this.hash = bcrypt.hashSync(this.password, salt);

    }
    checkpass(comphash) {
        this.comphash = comphash;
        // comparing password and hash
        this.compare = bcrypt.compareSync(this.password, this.comphash);
        return this.compare;
    }
    returnhash() {
        return this.hash;
    }
}

module.exports = PassCode;
