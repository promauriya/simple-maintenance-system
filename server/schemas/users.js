const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String, required: true },
    fleet: { type: String, required: true },
    permission: { type: Number, required: true, default: 1 },
    status: { type: Boolean, required: true, default: false },
    dateofcreate: { type: Date, default: Date.now }

});

module.exports = mongoose.model('User',userSchema);