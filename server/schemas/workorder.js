const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const workSchema = new Schema({
    fleet: { type: String, required: true },
    assigneeID: { type: String, required: true },
    vehicleID: { type: String, required: true },
    description: { type: String },
    cost: { type: String, required: true },
    status: { type: String, required: true, default: 'open' },
    lastupdated: { type: Date, default: Date.now },
    dateofcreate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('WorkOrder',workSchema);