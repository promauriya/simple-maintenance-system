const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
    // Checking token in header
    const token = req.header("x-auth-token");
    if (!token) return res.status(401).send({
        status: false,
        error: "No token available. Access denied."
    });

    try {
        const decoded = jwt.verify(token, process.env.PRIVATE_KEY);
        req.user = decoded;
    } catch (error) {
        return res.status(401).send({
            status: false,
            error: "Token expired or not valid."
        });
    }
    next();
}