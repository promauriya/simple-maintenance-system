function manager(req, res, next) {
    if (req.user.permission !== 3) return res.status(403).send({
        status: false,
        error: "Access denied."
    });

    next();
}

function technician(req, res, next) {
    if (req.user.permission < 2) return res.status(403).send({
        status: false,
        error: "Access denied."
    });

    next();
}

function driver(req, res, next) {
    if (!req.user.permission) return res.status(403).send({
        status: false,
        error: "Access denied."
    });

    next();
}

module.exports = { manager, technician, driver };