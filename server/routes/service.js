const express = require("express");

const mongoose = require('mongoose');
const workSchema = require('../schemas/workorder');

// Import from middlewares
const auth = require("../middlewares/auth");
const { manager, technician, driver } = require("../middlewares/permission");
// const { findUser, createUpdateWork, deleteWork } = require("../middlewares/dbcalls");

// Sample data
let data = [{ message:"Data successful." }];

// Setup the router for express
const router = express.Router();

// Routers handlers
router.get("/get", [auth, driver], (req, res, next) => {
    res.status(200).send({
        status: true,
        result: data
    });
});

// Get work orders.
router.post("/get", [auth, driver], (req, res, next) => {
    // Get work order
    let findBy = {
        fleet: req.user.fleet
    };
    
    // Skip number
    let skipnumber = 0;
    if(req.body.skip) { skipnumber = req.body.skip; }

    //IF _id is present
    if(req.body._id) { findBy._id = req.body._id; }

    // If vehical id avaibale
    if(req.body.vehicleid) { findBy.vehicleID = req.body.vehicleid; }

    // If status exist
    if(req.body.status) { findBy.status = req.body.status; }

    workSchema.find( findBy ).sort({ lastupdated: 'desc' }).skip(skipnumber).limit(20).exec( ( err, returndata ) => {
        // if error
        if(err) { return res.status(500).send({ status: false, message: err.message }); }
        // if data
        if(returndata) { return res.status(200).send({ status: true, data: returndata }); }
    });
});

// Create Work order
router.post("/create", [auth, technician], async (req, res, next) => {
    // find if something missing
    if( !req.body.vehicleid || !req.body.cost || !req.body.description || !req.body.status ) { return res.status(400).send({status: false, message: 'Please enter all required details.'}); }

    // Creating data structure
    let userdata = req.user;
    let data = {
        assigneeID: userdata.email,
        fleet: userdata.fleet,
        vehicleID: req.body.vehicleid,
        description: req.body.description,
        cost: req.body.cost,
        status: req.body.status
    };

    // If status exist
    if(req.body.status) { data.status = req.body.status; }

    // Save data
    let workEntry = new workSchema(data);
    workEntry.save((error) => {
        if(error) {
            return res.status(500).send({status:false, message: error.message});
        } else {
            return res.status(200).send({status:true, message: 'Work entry saved.'});
        }
    });
});

// Update work order
router.put("/update", [auth, technician], async (req, res, next) => {
    // if _id information is missing
    if(!req.body._id) { return res.status(400).send({status: false, message: 'No work order selected.'}); }
    // Data Structure
    let data = {
        lastupdated: Date.now()
    };
    // Push data if available
    if(req.body.description) { data.description = req.body.description; }
    if(req.body.cost) { data.cost = req.body.cost; }
    if(req.body.status) { data.status = req.body.status; }

    // Update Work Order
    workSchema.findOneAndUpdate({ _id: req.body._id, fleet: req.user.fleet }, data, { new: true, maxTimeMS: 500, runValidators: true }, (err,returndata) => {
        if(err) { return res.status(500).send({ status: false, message: err.message }); }
        if(returndata) { return res.status(200).send({ status: true, message: 'Work order updated.'}); }
    });
});


// Delete work order
router.delete("/delete", [auth, manager], async (req, res, next) => {
    // if the _id is not present in request
    if(!req.body._id) { return res.status(400).send({status: false, message: 'No work order selected.'}); }
    // Delete by _id and if fleet matches
    workSchema.findOneAndDelete({ fleet: req.user.fleet, _id: req.body._id }, function (err, docs) {
        if(err){ return res.status(500).send({ status: false, message: err.message }); }
        if(docs){ return res.status(200).send({ status: true, message: "Work order deleted." }); }
        if(docs === null) { return res.status(404).send({ status: false, message: "No work order to delete." }); }
    });
});

//ROuter export
module.exports = router;