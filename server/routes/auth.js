const jwt = require("jsonwebtoken");
const express = require("express");
const PassCode = require('../class/passcode');
const userSchema = require('../schemas/users');

// Setup the express server router
const router = express.Router();

// Auth post
router.post("/login", async (req, res, next) => {
    // If parameter not set
    if(!req.body.email || !req.body.password) { return res.status(400).send({login: false, message: 'Please enter email and password.'});}
    userSchema.findOne({email: req.body.email,status: true},(err, udata) => {
        // if any error
        if(err) { return res.status(500).send({login: false, message: err.message}); }
        // If no user
        if(udata === null) { return res.status(401).send({login: false, message: 'Email not found.'}); }
        // If user found
        if(udata) {
            // Compare password
            let compaired = new PassCode(req.body.password);
            if(compaired.checkpass(udata.password) === true) {
                // Password match
                // Token expires in 6 hours
                const token = jwt.sign({
                    email: udata.email,
                    permission: udata.permission,
                    fleet: udata.fleet,
                }, process.env.PRIVATE_KEY, { expiresIn: "6h" });

                return res.status(202).send({
                    login: true,
                    message: 'Login successful.',
                    token: token,
                    name: udata.name,
                    permission: udata.permission
                });

            } else {
                // if wrong password
                return res.status(403).send({login: false, message:'Wrong password.'});
            }

        }
    });
});


// // Creating user for testing only
// router.post("/user/create", async (req, res, next) => {
//     var genPass = new PassCode(req.body.password);
//     let theEntry = {
//         password: genPass.returnhash(),
//         email: req.body.email,
//         name: req.body.name,
//         fleet: req.body.fleet,
//         permission: req.body.permission,
//         status: req.body.status
//     };
//     var userEntry = new userSchema(theEntry);
//     userEntry.save((error) => {
//         if(error) {
//             console.log(error);
//             res.status(500).json({status:false});
//         } else {
//             res.status(201).json({status:true});
//         }
//     });
// });

// Export the router
module.exports = router;