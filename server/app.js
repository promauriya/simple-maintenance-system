// Getting .env variables
require('dotenv').config();

const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');

// Router
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

// custom routes
const authRouter = require('./routes/auth');
const serviceRouter = require('./routes/service');

// Cors for no cross domain issue
const cors = require('cors');

const app = express();

// Using CORS
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json({limit:"20mb"}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// MongoDB connection. Taking static data from .env file
mongoose.connect(process.env.DATABASE_URL);
const db = mongoose.connection;

// Check Connection
db.once('open', function(){
  console.log('Connected to MongoDB')
});

// Check for DB errors
db.on('error', function(err){
  console.log(err);
});




app.use('/', indexRouter);
app.use('/users', usersRouter);

// Custom route
app.use('/api/auth', authRouter);
app.use('/api/service', serviceRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
