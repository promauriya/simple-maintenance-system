const request = require('supertest');
const server = require('../app');

// Login Test
it('Login Post testing', (done)=> {
    request(server)
    .post('/api/auth/login')
    .send({email:'admin@test.com',password:'testadmin'})
    .then((response)=>{
        expect(response).toBe(200);
        done();
    });
})

// Get Service Provider
it('Get Work order testing', (done)=> {
    request(server)
    .post('/api/service/get')
    .then((response)=>{
        expect(response).toBe(200);
        done();
    });
})

