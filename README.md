# simplemaintenance

Simple maintenance management system for certain industries where total cost of ownership or total operating costs is a substantial percentage of revenue, the maintenance process should be tracked in order to minimize costs of assets.


# User Credentials
## User Driver
Email: `driver@test.com`\
Password: `testdriver`
## User Technician
Email: `technician@test.com`\
Password: `testtechnician`
## User manager
Email: `admin@test.com`\
Password: `testadmin`


# Server

## Instalation
Please clone the repo with git. Command: `git clone -b <branch> <Repo link>`\
Please go to folder server and run the following command: `npm install`\
Please make sure all the dependencies as=re installed with no critical errors. `npm audit fix`\
Please change the PRIVATE_KEY & DATABASE_URL if you want inside `.env` file.

## Development Server
Please run command: `nodemon start` or `npm start` to start the server.

## Production Server
Please install PM2 in the server. command: `npm install pm2 -g`\
if you want to run your server in cluster on all of the cores of your Server. `pm2 start ./bin/www -i max`\
if you want to run only one instance of the server: `pm2 start ./bin/www`\
If you want to determine the number of instances in cluster to run: `pm2 start ./bin/www -i <number>`\
Stopping the cluster: `pm2 stop all`\
Monitor all instances: `pm2 monit`\
Stopping particular instance: `pm2 stop <name>`\
Delete particulr instance: `pm2 delete <name>`\
See the log: `pm2 log`

## Running load test
Install module: `npm install -g loadtest`\
Load testing with certain number of requests: `loadtest -n 3000 <server api link>`\
Load testing with number of client and number of request per second: `loadtest -c 10 --rps 200 <server api link>`

# Client

## Installation
Please run `npm install --save` in client folder\


## Development
Replace server address in client/src/config/const.js folder.\
Please run `npm start`



## Production
Server setup\
In root directory please run `nano etc/nginx/sites-available/default`\
Replace all code with \
```
server {
  listen 80;
  server_name _;
  root /var/www/html/simple-maintenance-system/client/build;
  index index.html;
  # Other config you desire (TLS, logging, etc)...
  location / {
    try_files $uri /index.html;
  }
}
```
Save it.\

Please run `npm run build`\
Please restart the nginx `systemctl restart nginx`



